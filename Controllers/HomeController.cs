﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ceritek.WebServices.DB.Controllers
{
    public class HomeController : Controller
    {
        public RedirectResult Index()
        {
            return Redirect(
                Global.virtualDirectory +
                (
                    "/".Equals(Global.virtualDirectory)
                        ? String.Empty
                        : "/"
                ) +
                "Sql.aspx"
                );
            // ViewBag.Title = "Home Page";

            //  return View();
        }
    }
}
