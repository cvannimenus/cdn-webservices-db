﻿using Ceritek.WebServices.DB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Ceritek.WebServices.DB
{
    public class JSONContentTypeAtttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.Request.Method == HttpMethod.Post
                || actionContext.Request.Method == HttpMethod.Put
                || actionContext.Request.Method == HttpMethod.Delete)
            {
                ErrorMessage errorM = new ErrorMessage();
                errorM.errorMessage = "Bad Content-Type";
                try
                {
                    bool correct = false;
                    if (actionContext.Request.Content != null)
                    {
                        string lContentType = actionContext.Request.Content.Headers.GetValues("Content-Type").First();
                        if (lContentType!=null && lContentType.Contains("application/json"))
                            correct = true;
                    }
                    if (!correct)
                    {

                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                            errorM);
                    }
                }
                catch
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                            errorM);
                }
            }
        }
    }
}