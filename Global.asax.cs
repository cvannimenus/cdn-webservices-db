using Ceritek.WebServices.DB.Logic;
using Ceritek.WebServices.DB.Models;
using log4net;
using log4net.Config;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Ceritek.WebServices.DB
{
    public class Global : System.Web.HttpApplication
    {
        public static bool bouchon = false;

        private static readonly ILog logger = LogManager.GetLogger(typeof(Global));
        public static bool serviceStarted = false;
        private static Thread batchWorkerThread;
        private static ManualResetEvent _event = new ManualResetEvent(true);
        private static string urlping;

        public static string virtualDirectory { get => ConfigurationManager.AppSettings.Get("virtualDirectory"); }

        public static string CustomerHistory_connString
        {
            get
            {
                if (Global.bouchon)
                   return "server=AIUR\\SQLEXPRESS,1414; uid=crm;pwd=crm;database=CRM;Connection Timeout=5";
                return ConfigurationManager.ConnectionStrings["CRMDB"].ToString();
            }
        }

        public static string HttpToken {
            get
            {
                string passRead = (string)ConfigurationManager.AppSettings.Get("token");
                string passPhrase = "BHTH32RV2R2HJUER82FU2V4F";
                if (!passRead.StartsWith("CKSH0056")) // Need to encrypt
                {
                    string encryptedPass = Cypher.Encrypt(passRead, passPhrase);
                    Configuration config = WebConfigurationManager.OpenWebConfiguration(Global.virtualDirectory);
                    var settings = config.AppSettings.Settings;
                    passRead = "CKSH0056" + encryptedPass;
                    settings["token"].Value = passRead;
                    config.Save(ConfigurationSaveMode.Modified);
                }
                string encodedPass = Cypher.Decrypt(passRead.Replace("CKSH0056", ""), passPhrase);
                return Encoding.UTF8.GetString(Convert.FromBase64String(encodedPass));
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            urlping = Request.Url.GetLeftPart(UriPartial.Authority) + ("/".Equals(Global.virtualDirectory) ? String.Empty : Global.virtualDirectory) + "/Ping.aspx";
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            XmlConfigurator.Configure(new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + "Log4Net.xml"));
            WriteLogs(LogLevel.INFO, "Main", "Application starting....");
            WriteLogs(LogLevel.INFO, "Main", "Version: " + Assembly.GetExecutingAssembly().GetName().Version.ToString());

            ParameterizedThreadStart start = new ParameterizedThreadStart(BatchWorker);
            batchWorkerThread = new Thread(start);
            serviceStarted = true;
            batchWorkerThread.Start();

            WriteLogs(LogLevel.INFO, "Main", "Application started");
        }

        static void BatchWorker(object ob)
        {
            for (int i = 0; i < 120; i++)
            {
                Thread.Sleep(1000);
                _event.WaitOne();
            }
            // Check the thread status (started?)
            while (serviceStarted)
            {
                try
                {
                    // Check the thread status (suspended?)
                    _event.WaitOne();
                    //   var jobtest= Task.Run(() => JobTest());
                }
                catch (Exception e)
                {
                    WriteLogs(LogLevel.ERROR, "BatchWorker", "Exception: " + e);
                    WriteLogs(LogLevel.INFO, "BatchWorker", "Restarting in 1 minute...");
                }
                for (int i = 0; i < 60; i++)
                {
                    Thread.Sleep(1000);
                    _event.WaitOne();
                }
            }

        }

        protected void Application_End(object sender, EventArgs e)
        {
            WriteLogs(LogLevel.INFO, "Main", "Application ending...");
            serviceStarted = false;

            WriteLogs(LogLevel.INFO, "Main", "Application ended");

            if(!string.IsNullOrEmpty(urlping))
            {
                var client = new WebClient();
                client.DownloadString(urlping);
                Trace.WriteLine("Application Shut Down Ping: " + urlping);
            }

        }

        public enum LogLevel
        {
            DEBUG = 0,
            INFO = 1,
            WARNING = 2,
            ERROR = 3
        }

        public static void WriteLogs(LogLevel level, string module, string message)
        {
            String msgToDisplay = "[" + level + "] @" + module + " " + message;
            switch (level)
            {
                case LogLevel.DEBUG:
                    logger.Debug(msgToDisplay);
                    break;
                case LogLevel.ERROR:
                    logger.Fatal(msgToDisplay);
                    break;
                case LogLevel.INFO:
                    logger.Info(msgToDisplay);
                    break;
                case LogLevel.WARNING:
                    logger.Warn(msgToDisplay);
                    break;
            }
            System.Diagnostics.Debug.WriteLine(msgToDisplay);
        }

    }

}
