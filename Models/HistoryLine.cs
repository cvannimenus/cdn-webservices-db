﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace Ceritek.WebServices.DB.Models
{
    public class HistoryLine
    {
        [JsonProperty("myday")]
        public string MyDay { get; set; }
        [JsonProperty("mytime")]
        public string MyTime{ get; set; }
        [JsonProperty("callauth")]
        public bool CallAuth { get; set; } = false;

        public HistoryLine(string _MyDay, string _MyTime, bool _CallAuth)
        {
            MyDay = _MyDay;
            MyTime = _MyTime;
            CallAuth = _CallAuth;
        }
        public HistoryLine()
        {
        }

        public override string ToString()
        {
            return Json.Encode(this);
        }
    }
}