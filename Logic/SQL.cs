﻿using Ceritek.WebServices.DB.Models;

using System;
using System.Collections.Generic;
using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;
using System.Data.Common;
using System.Web.Http;

namespace Ceritek.WebServices.DB.Logic
{
    public sealed class SQL
    {
        private static SQL instance = null;
        private static readonly object padlock = new object();

        SQL()
        {
        }

        public static SQL Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new SQL();
                    }
                    return instance;
                }
            }
        }

        public bool setAttributes(string connid, SetAttributes attribs)
        {
            bool ret = false;
            string strAttributes = String.Empty;
            if (!string.IsNullOrEmpty(attribs.authent_amer))
            {
                strAttributes += (String.IsNullOrEmpty(strAttributes) ? " " : ", ");
                strAttributes += "AUTHENT_AMER='" + attribs.authent_amer + "'";
            }
            if (!string.IsNullOrEmpty(attribs.client_delicat))
            {
                strAttributes += (String.IsNullOrEmpty(strAttributes) ? " " : ", ");
                strAttributes += "CLIENT_DELICAT='" + attribs.client_delicat + "'";
            }
            if (!string.IsNullOrEmpty(attribs.stop_bvo_asked))
            {
                strAttributes += (String.IsNullOrEmpty(strAttributes) ? " " : ", ");
                strAttributes += "STOP_BVO_ASKED='" + attribs.stop_bvo_asked + "'";
            }
            if (!string.IsNullOrEmpty(attribs.delete_print_asked))
            {
                strAttributes += (String.IsNullOrEmpty(strAttributes) ? " " : ", ");
                strAttributes += "DELETE_PRINT_ASKED='" + attribs.delete_print_asked + "'";
            }

            if (Global.bouchon)
            {
                SqlConnection conn = new SqlConnection(Global.CustomerHistory_connString);
                if (conn == null)
                {
                    LogInfo("Error: Impossible de se connecter à la base de données");
                }
                else
                {
                    try
                    {


                        // Connexion à la base de données
                        conn.Open();
                        string sql = "UPDATE CUSTOMER_HISTORY SET" + strAttributes + " " +
                                   "WHERE (CALL_CONNID=@connid) ";
                        SqlCommand cmd = new SqlCommand(sql, conn);

                        SqlParameter param1 = new SqlParameter();
                        param1.ParameterName = "connid";
                        param1.Value = connid.ToLower();
                        cmd.Parameters.Add(param1);

                        int nb = cmd.ExecuteNonQuery();
                        if (nb > 0)
                            ret = true;

                    }
                    catch (Exception sqlE)
                    {
                        LogInfo("Error: " + sqlE.Message);
                        LogInfo(sqlE.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            else
            {
                OracleConnection conn = new OracleConnection(Global.CustomerHistory_connString);
                if (conn == null)
                {
                    LogInfo("Error: Impossible de se connecter à la base de données");
                }
                else
                {
                    try
                    {
                        // Connexion à la base de données
                        conn.Open();
                        string sql = "UPDATE CUSTOMER_HISTORY SET" + strAttributes + " " +
                                   "WHERE (CALL_CONNID=:connid) ";

                        OracleCommand cmd = new OracleCommand(sql, conn);
                        OracleParameter param1 = new OracleParameter();
                        param1.ParameterName = "connid";
                        param1.Value = connid.ToLower();
                        cmd.Parameters.Add(param1);

                        int nb = cmd.ExecuteNonQuery();
                        if (nb > 0)
                            ret = true;
                    }
                    catch (Exception sqlE)
                    {
                        LogInfo("Error: " + sqlE.Message);
                        LogInfo(sqlE.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }

            return ret;
        }

        public bool setAuth(string compte, SetAuth body)
        {
            bool ret = false;
            if (Global.bouchon)
            {
                SqlConnection conn = new SqlConnection(Global.CustomerHistory_connString);
                if (conn == null)
                {
                    LogInfo("Error: Impossible de se connecter à la base de données");
                }
                else
                {
                    try
                    {
                        // Connexion à la base de données
                        conn.Open();
                        string sql = "UPDATE CUSTOMER_HISTORY SET CALL_AUTH=@auth, CALL_AUTH_TYPE=@authtype " +
                                       "WHERE (CUST_ACCOUNT=@compte)   " +
                                       "AND CALL_DATE = " +
                                       "(SELECT MAX(CALL_DATE) FROM CUSTOMER_HISTORY " +
                                       "WHERE (CUST_ACCOUNT=@compte)) ";
                        SqlCommand cmd = new SqlCommand(sql, conn);

                        SqlParameter param1 = new SqlParameter();
                        param1.ParameterName = "auth";
                        param1.Value = body.auth;
                        cmd.Parameters.Add(param1);

                        SqlParameter param2 = new SqlParameter();
                        param2.ParameterName = "authtype";
                        param2.Value = body.authtype;
                        cmd.Parameters.Add(param2);

                        SqlParameter param3 = new SqlParameter();
                        param3.ParameterName = "numero";
                        param3.Value = body.numero;
                        cmd.Parameters.Add(param3);

                        SqlParameter param4 = new SqlParameter();
                        param4.ParameterName = "compte";
                        param4.Value = compte;
                        cmd.Parameters.Add(param4);

                        int nb = cmd.ExecuteNonQuery();
                        if (nb > 0)
                            ret = true;

                    }
                    catch (Exception sqlE)
                    {
                        LogInfo("Error: " + sqlE.Message);
                        LogInfo(sqlE.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            else
            {
                OracleConnection conn = new OracleConnection(Global.CustomerHistory_connString);
                if (conn == null)
                {
                    LogInfo("Error: Impossible de se connecter à la base de données");                }
                else
                {
                    try
                    {
                        // Connexion à la base de données
                        conn.Open();

                        // Lecture en base
                        string sql = "UPDATE CUSTOMER_HISTORY SET CALL_AUTH=:auth, CALL_AUTH_TYPE=:authtype " +
                                       "WHERE (CUST_ACCOUNT=:compte) " +
                                       "AND CALL_DATE = " +
                                       "(SELECT MAX(CALL_DATE) FROM CUSTOMER_HISTORY " +
                                       "WHERE (CUST_ACCOUNT=:compte)) ";

                        OracleCommand cmd = new OracleCommand(sql, conn);
                        OracleParameter param1 = new OracleParameter();
                        param1.ParameterName = "auth";
                        param1.Value = body.auth;
                        cmd.Parameters.Add(param1);

                        OracleParameter param2 = new OracleParameter();
                        param2.ParameterName = "authtype";
                        param2.Value = body.authtype;
                        cmd.Parameters.Add(param2);

                        OracleParameter param3 = new OracleParameter();
                        param3.ParameterName = "numero";
                        param3.Value = body.numero;
                        cmd.Parameters.Add(param3);

                        OracleParameter param4 = new OracleParameter();
                        param4.ParameterName = "compte";
                        param4.Value = compte;
                        cmd.Parameters.Add(param4);

                        int nb = cmd.ExecuteNonQuery();
                        if (nb > 0)
                            ret = true;
                    }
                    catch (Exception sqlE)
                    {
                        LogInfo("Error: " + sqlE.Message);
                        LogInfo(sqlE.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }

            return ret;
        }

        public List<HistoryLine> getHistory(string compte)
        {
            List<HistoryLine> ret = new List<HistoryLine>();
            if(Global.bouchon)
            {
                SqlConnection conn = new SqlConnection(Global.CustomerHistory_connString);
                if (conn == null)
                {
                    LogInfo("Error: Impossible de se connecter à la base de données");
                    ret = null;
                }
                else
                {
                    try
                    {
                        // Connexion à la base de données
                        conn.Open();

                        // Lecture en base
                        string sql = "SELECT TOP 10 convert(varchar, CALL_DATE, 103) As MY_DAY, convert(varchar, CALL_DATE, 8) As MY_TIME, CALL_AUTH " +
                                   "FROM CUSTOMER_HISTORY " +
                                   "WHERE (CUST_ACCOUNT=@compte) " +
                                   "ORDER BY CALL_DATE DESC ";
                        SqlCommand cmd = new SqlCommand(sql, conn);

                        SqlParameter param1 = new SqlParameter();
                        param1.ParameterName = "compte";
                        param1.Value = compte;
                        cmd.Parameters.Add(param1); 
                        
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    string myday = Convert.ToString(reader.GetValue(reader.GetOrdinal("MY_DAY")));
                                    string mytime = Convert.ToString(reader.GetValue(reader.GetOrdinal("MY_TIME")));
                                    bool callauth = false;
                                    string callauthS = String.Empty;
                                    try
                                    {
                                        callauthS = Convert.ToString(reader.GetValue(reader.GetOrdinal("CALL_AUTH")));
                                    }
                                    catch
                                    {
                                        callauthS = null;
                                    }

                                    if (callauthS == null)
                                    {
                                        callauth = false;
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(callauthS) || callauthS.Contains("No"))
                                        {
                                            callauth = false;
                                        }
                                        else
                                        {
                                            callauth = true;
                                        }
                                    }

                                    HistoryLine histo = new HistoryLine
                                    {
                                        MyDay = myday,
                                        MyTime = mytime,
                                        CallAuth = callauth
                                    };

                                    ret.Add(histo);
                                }
                            }
                        }
                    }
                    catch (Exception sqlE)
                    {
                        LogInfo("Error: " + sqlE.Message);
                        LogInfo(sqlE.StackTrace);
                        ret = null;
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            else
            {
                OracleConnection conn = new OracleConnection(Global.CustomerHistory_connString);
                if (conn == null)
                {
                    LogInfo("Error: Impossible de se connecter à la base de données");
                    ret = null;
                }
                else
                {
                    try
                    {
                        // Connexion à la base de données
                        conn.Open();

                        // Lecture en base
                        string sql = "SELECT * FROM(" +
                                       "SELECT to_char(CALL_DATE, 'DD/MM/YYYY') As MY_DAY,  to_char(CALL_DATE, 'HH24:MI:SS') As MY_TIME, CALL_AUTH " +
                                       "FROM CUSTOMER_HISTORY " +
                                       "WHERE (CUST_ACCOUNT=:compte) " +
                                       "ORDER BY CALL_DATE DESC " +
                                       ") WHERE ROWNUM<=10";
                        OracleCommand cmd = new OracleCommand(sql, conn);
                        OracleParameter param1 = new OracleParameter();
                        param1.ParameterName = "compte";
                        param1.Value = compte;
                        cmd.Parameters.Add(param1);

                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    string myday = Convert.ToString(reader.GetValue(reader.GetOrdinal("MY_DAY")));
                                    string mytime = Convert.ToString(reader.GetValue(reader.GetOrdinal("MY_TIME")));
                                    bool callauth = false;
                                    string callauthS = String.Empty;
                                    try
                                    {
                                        callauthS = Convert.ToString(reader.GetValue(reader.GetOrdinal("CALL_AUTH")));
                                    }
                                    catch
                                    {
                                        callauthS = null;
                                    }

                                    if (callauthS == null)
                                    {
                                        callauth = false;
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(callauthS) || callauthS.Contains("No"))
                                        {
                                            callauth = false;
                                        }
                                        else
                                        {
                                            callauth = true;
                                        }
                                    }

                                    HistoryLine histo = new HistoryLine
                                    {
                                        MyDay = myday,
                                        MyTime = mytime,
                                        CallAuth = callauth
                                    };

                                    ret.Add(histo);          
                                }
                            }
                        }
                    }
                    catch (Exception sqlE)
                    {
                        LogInfo("Error: " + sqlE.Message);
                        LogInfo( sqlE.StackTrace);
                        ret = null;
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            
            
            return ret;
        }

        public List<RepertoireLine> getDirectory(string code)
        {
            List<RepertoireLine> ret = new List<RepertoireLine>();
            if (Global.bouchon)
            {
                SqlConnection conn = new SqlConnection(Global.CustomerHistory_connString);
                if (conn == null)
                {
                    LogInfo("Error: Impossible de se connecter à la base de données");
                    ret = null;
                }
                else
                {
                    try
                    {
                        // Connexion à la base de données
                        conn.Open();

                        // Lecture en base
                        string sql = "SELECT LABEL, PHONENUMBER from V_DIRECTORY " +
                                        "WHERE FILTER=@code "+
                                        "ORDER BY ENTRY_PRIORITY ASC, LABEL ASC";

                        SqlCommand cmd = new SqlCommand(sql, conn);

                        SqlParameter param1 = new SqlParameter();
                        param1.ParameterName = "code";
                        param1.Value = code;
                        cmd.Parameters.Add(param1);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    string label = Convert.ToString(reader.GetValue(reader.GetOrdinal("LABEL")));
                                    string phonenumber = Convert.ToString(reader.GetValue(reader.GetOrdinal("PHONENUMBER")));
                                   
                                    RepertoireLine rep = new RepertoireLine
                                    {
                                        Label = label,
                                        Phone = phonenumber
                                    };

                                    ret.Add(rep);
                                }
                            }
                        }
                    }
                    catch (Exception sqlE)
                    {
                        LogInfo("Error: " + sqlE.Message);
                        LogInfo(sqlE.StackTrace);
                        ret = null;
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            else
            {
                OracleConnection conn = new OracleConnection(Global.CustomerHistory_connString);
                if (conn == null)
                {
                    LogInfo("Error: Impossible de se connecter à la base de données");
                    ret = null;
                }
                else
                {
                    try
                    {
                        // Connexion à la base de données
                        conn.Open();

                        // Lecture en base
                        string sql = "SELECT LABEL, PHONENUMBER from V_DIRECTORY " +
                                        "WHERE FILTER=:code " +
                                        "ORDER BY ENTRY_PRIORITY ASC, LABEL ASC";
                        OracleCommand cmd = new OracleCommand(sql, conn);
                        OracleParameter param1 = new OracleParameter();
                        param1.ParameterName = "code";
                        param1.Value = code;
                        cmd.Parameters.Add(param1);

                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    string label = Convert.ToString(reader.GetValue(reader.GetOrdinal("LABEL")));
                                    string phonenumber = Convert.ToString(reader.GetValue(reader.GetOrdinal("PHONENUMBER")));

                                    RepertoireLine rep = new RepertoireLine
                                    {
                                        Label = label,
                                        Phone = phonenumber
                                    };

                                    ret.Add(rep);
                                }
                            }
                        }
                    }
                    catch (Exception sqlE)
                    {
                        LogInfo("Error: " + sqlE.Message);
                        LogInfo(sqlE.StackTrace);
                        ret = null;
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }


            return ret;
        }

        void LogInfo(string mess)
        {
            Global.WriteLogs(Global.LogLevel.INFO, "SQL", mess);
        }
    }
}