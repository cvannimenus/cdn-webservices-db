﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace Ceritek.WebServices.DB.Models
{
    public class History
    {
        [JsonProperty("history")]
        public List<HistoryLine> history { get; set; }

        public History(List<HistoryLine> _history)
        {
            history = _history;
        }
        public History()
        {
            history = new List<HistoryLine>();
        }

        public override string ToString()
        {
            return Json.Encode(this);
        }
    }
}