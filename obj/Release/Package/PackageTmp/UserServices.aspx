﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserServices.aspx.cs" Inherits="Ceritek.WebServices.Biometrie.UserServices" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Services (<%=global_asax.certificat%>)</title>
    <style type="text/css">
		
body{
	margin: 0;
	padding: 0;
	font-family: Arial;
	font-size: 12px;
}

.body{
	position: absolute;
	top: -20px;
	left: -20px;
	right: -40px;
	bottom: -40px;
	width: auto;
	height: auto;
	background-image: url(img/data-security.jpg);
	background-size: cover;
	z-index: 0;
}
.form-style-7{
	max-width:400px;
	margin:50px auto;
	border-radius:2px;
	padding:20px;
	font-family: Georgia, "Times New Roman", Times, serif;
}
.form-style-7 h1{
	display: block;
	text-align: center;
	padding: 0;
	margin: 0px 0px 20px 0px;
	color: #FFFFFF;
	font-size:x-large;
}
.form-style-7 ul{
	list-style:none;
	padding:0;
	margin:0;	
}
.form-style-7 li{
	display: block;
	padding: 9px;
	border:1px solid #DDDDDD;
	margin-bottom: 30px;
	border-radius: 3px;
}
.form-style-7 li:last-child{
	border:none;
	margin-bottom: 0px;
	text-align: center;
}
.form-style-7 li > label{
	display: block;
	float: left;
	margin-top: -19px;
	height: 14px;
	padding: 2px 5px 2px 5px;
	color: #B9B9B9;
	font-size: 14px;
	overflow: hidden;
	font-family: Arial, Helvetica, sans-serif;
}
.form-style-7 input[type="text"],
.form-style-7 input[type="date"],
.form-style-7 input[type="datetime"],
.form-style-7 input[type="email"],
.form-style-7 input[type="number"],
.form-style-7 input[type="search"],
.form-style-7 input[type="time"],
.form-style-7 input[type="url"],
.form-style-7 input[type="password"],
.form-style-7 textarea,
.form-style-7 select 
{
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	background : transparent;
	width: 100%;
	display: block;
	outline: none;
	border: none;
	color : #FFFFFF;
	height: 25px;
	line-height: 25px;
	font-size: 16px;
	padding: 0;
	font-family: Georgia, "Times New Roman", Times, serif;
}
.form-style-7 input[type="text"]:focus,
.form-style-7 input[type="date"]:focus,
.form-style-7 input[type="datetime"]:focus,
.form-style-7 input[type="email"]:focus,
.form-style-7 input[type="number"]:focus,
.form-style-7 input[type="search"]:focus,
.form-style-7 input[type="time"]:focus,
.form-style-7 input[type="url"]:focus,
.form-style-7 input[type="password"]:focus,
.form-style-7 textarea:focus,
.form-style-7 select:focus 
{
}
.form-style-7 li > span{
	background: #F3F3F3;
	display: block;
	padding: 3px;
	margin: 0 -9px -9px -9px;
	text-align: center;
	color: #FFFFFF;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.form-style-7 textarea{
	resize:none;
}
.form-style-7 input[type="submit"],
.form-style-7 input[type="button"]{
	background: #2471FF;
	border: none;
	padding: 10px 20px 10px 20px;
	border-bottom: 3px solid #5994FF;
	border-radius: 3px;
	color: #D2E2FF;
}
.form-style-7 input[type="submit"]:hover,
.form-style-7 input[type="button"]:hover{
	background: #6B9FFF;
	color:#fff;
}

.tab td {
	background-color:#ffffff;
	opacity:0.5;
	color:#000000;
	font-size: 25px;
}
</style>
	<script type="text/javascript">
		function detect_type(obj_input) {
			var regex_idpersonne = RegExp("^[0-9]{18}$");
            var regex_alias = RegExp('^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$');
            var regex_etoile_direct = new RegExp("^[0-9]{5,11}$");

            var span = document.getElementById("span_info");
			var hid = document.getElementById("hid_type");
			hid.value = "";
			span.textContent = "";

			if (obj_input != null && obj_input.value != null) {
				if (regex_idpersonne.test(obj_input.value)) {
                    span.textContent = "ID Personne";
					hid.value = "Personne";
				}
				else if (regex_alias.test(obj_input.value)) {
                    span.textContent = "ID Alias";
					hid.value = "Alias";
				}
				else if (regex_etoile_direct.test(obj_input.value)) {
                    span.textContent = "ID Etoile Direct";
                    hid.value = "Etoile";
                }
			}
		}
</script>
</head>
<body>
	  <div class="body">
	<form class="form-style-7" runat="server">
	<h1>Biométrie</h1>
	<input type="hidden" name="hid_type" id="hid_type" runat="server"/>
	<ul>
	<li>
		<input type="text" id="identifiant" name="identifiant" placeholder="identifiant" maxlength="100" onkeyup="detect_type(this)" runat="server" />
		<span style="color:black;">Merci de fournir l'identifiant en votre possession</span>
		<span style="color:green;font-size:20px" id="span_info"><asp:Literal ID="infotext" runat="server"></asp:Literal></span>
	</li>
	<li>
		<input type="submit" name="button_submit" value="Rechercher" runat="server" onServerClick="Search_Click"/>
	</li>
	</ul>
</form>

<%if (!String.IsNullOrEmpty(userservices_aspx.result.ID_Personne) || !String.IsNullOrEmpty(userservices_aspx.result.ID_Alias) || !String.IsNullOrEmpty(userservices_aspx.result.ID_Personne) )
    { %>
	<div style="position: absolute;	left:40%;">
<table class="tab" >
	<tr><td>&nbsp;ID Personne: </td><td><%=userservices_aspx.result.ID_Personne %></td></tr>
	<tr><td>&nbsp;ID Alias: </td><td><%=userservices_aspx.result.ID_Alias %></td></tr>
	<tr><td>&nbsp;ID Etoile: </td><td><%=userservices_aspx.result.ID_Etoile %></td></tr>
</table>
		</div>
<%} %>
	</div>
</body>
</html>
