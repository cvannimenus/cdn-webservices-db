﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace Ceritek.WebServices.DB.Models
{
    public class RepertoireLine
    {
        [JsonProperty("label")]
        public string Label { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }

        public RepertoireLine(string _label, string _phone)
        {
            Label = _label;
            Phone = _phone;
        }
        public RepertoireLine()
        {
        }

        public override string ToString()
        {
            return Json.Encode(this);
        }
    }
}