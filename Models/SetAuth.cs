﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace Ceritek.WebServices.DB.Models
{
    public class SetAuth
    {
        [JsonProperty("numero")]
        public string numero { get; set; }
        [JsonProperty("compte")]
        public string compte { get; set; }
        [JsonProperty("auth")]
        public string auth { get; set; }
        [JsonProperty("authtype")]
        public string authtype { get; set; }

        public SetAuth(string _Numero, string _Compte, string _Auth, string _AuthType)
        {
            numero = _Numero;
            compte = _Compte;
            auth = _Auth;
            authtype = _AuthType;
        }
        public SetAuth()
        {
        }

        public override string ToString()
        {
            return Json.Encode(this);
        }
    }
}