﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminInfo.aspx.cs" Inherits="Ceritek.WebServices.Biometrie.AdminInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Admin - Info (<%=global_asax.certificat%>)</title>
    <style>
        body {
    background: #fafafa;
    color: #444;
    font: 100%/30px 'Helvetica Neue', helvetica, arial, sans-serif;
    text-shadow: 0 1px 0 #fff;
}

strong {
    font-weight: bold;
}

em {
    font-style: italic;
}

table {
    background: #f5f5f5;
    border-collapse: separate;
    box-shadow: inset 0 1px 0 #fff;
    font-size: 12px;
    line-height: 24px;
    margin: 30px auto;
    text-align: left;
    width: 800px;
}

th {
    background: linear-gradient(#777, #444);
    border-left: 1px solid #555;
    border-right: 1px solid #777;
    border-top: 1px solid #555;
    border-bottom: 1px solid #333;
    box-shadow: inset 0 1px 0 #999;
    color: #fff;
    font-weight: bold;
    padding: 10px 15px;
    position: relative;
    text-shadow: 0 1px 0 #000;
}

    th:after {
        background: linear-gradient(rgba(255,255,255,0), rgba(255,255,255,.08));
        content: '';
        display: block;
        height: 25%;
        left: 0;
        margin: 1px 0 0 0;
        position: absolute;
        top: 25%;
        width: 100%;
    }

    th:first-child {
        border-left: 1px solid #777;
        box-shadow: inset 1px 1px 0 #999;
    }

    th:last-child {
        box-shadow: inset -1px 1px 0 #999;
    }

td {
    border-right: 1px solid #fff;
    border-left: 1px solid #e8e8e8;
    border-top: 1px solid #fff;
    border-bottom: 1px solid #e8e8e8;
    padding: 10px 15px;
    position: relative;
    transition: all 300ms;
}

    td:first-child {
        box-shadow: inset 1px 0 0 #fff;
    }

    td:last-child {
        border-right: 1px solid #e8e8e8;
        box-shadow: inset -1px 0 0 #fff;
    }

tr:last-of-type td {
    box-shadow: inset 0 -1px 0 #fff;
}

    tr:last-of-type td:first-child {
        box-shadow: inset 1px -1px 0 #fff;
    }

    tr:last-of-type td:last-child {
        box-shadow: inset -1px -1px 0 #fff;
    }

tbody:hover td {
    color: transparent;
    text-shadow: 0 0 3px #aaa;
}

tbody:hover tr:hover td {
    color: #444;
    text-shadow: 0 1px 0 #fff;
}

    </style>
</head>
<body>
    <div style="text-align: center;">
        <span style="color:blue;font-size:20px">Services</span>
    </div>
<table>
  <thead>
    <tr>
      <th>Composant</th>
      <th>Derniere demande</th>
      <th>Date</th>
      <th>Résultat</th>
      <th>Message</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><strong>Nuance</strong></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.NUANCE.Instance.lastCall.Method %></td>
      <td><%=(Ceritek.WebServices.Biometrie.Logic.NUANCE.Instance.lastCall.Date==DateTime.MinValue?String.Empty:Ceritek.WebServices.Biometrie.Logic.NUANCE.Instance.lastCall.Date.ToString("dd/MM/yyyy HH:mm")) %></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.NUANCE.Instance.lastCall.Result %></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.NUANCE.Instance.lastCall.Message %></td>
    </tr>
    <tr>
      <td><strong>SSOP</strong></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.SSOP.Instance.lastCall.Method %></td>
      <td><%=(Ceritek.WebServices.Biometrie.Logic.SSOP.Instance.lastCall.Date==DateTime.MinValue?String.Empty:Ceritek.WebServices.Biometrie.Logic.SSOP.Instance.lastCall.Date.ToString("dd/MM/yyyy HH:mm")) %></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.SSOP.Instance.lastCall.Result %></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.SSOP.Instance.lastCall.Message %></td>
    </tr>
    <tr>
      <td><strong>SVI</strong></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.SVI.Instance.lastCall.Method %></td>
      <td><%=(Ceritek.WebServices.Biometrie.Logic.SVI.Instance.lastCall.Date==DateTime.MinValue?String.Empty:Ceritek.WebServices.Biometrie.Logic.SVI.Instance.lastCall.Date.ToString("dd/MM/yyyy HH:mm")) %></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.SVI.Instance.lastCall.Result %></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.SVI.Instance.lastCall.Message %></td>
    </tr>
    <tr>
      <td><strong>SQL</strong></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.SQL.Instance.lastCall.Method %></td>
      <td><%=(Ceritek.WebServices.Biometrie.Logic.SQL.Instance.lastCall.Date==DateTime.MinValue?String.Empty:Ceritek.WebServices.Biometrie.Logic.SQL.Instance.lastCall.Date.ToString("dd/MM/yyyy HH:mm")) %></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.SQL.Instance.lastCall.Result %></td>
      <td><%=Ceritek.WebServices.Biometrie.Logic.SQL.Instance.lastCall.Message %></td>
    </tr>					
  </tbody>
</table>
    <br />
    <br />
    <div style="text-align: center;">
        <span style="color:blue;font-size:20px">Batch</span>
    </div>
<table>
  <thead>
    <tr>
      <th>Service</th>
      <th>Etat</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><strong>Temps réél</strong></td>
      <td><%=(global_asax.serviceStarted?"Démarré":"Arrêté")%></td>
    </tr>
    <tr>
      <td><strong>Batch compte clos</strong></td>
      <td>Dernière execution : <%=(global_asax.lastExecutionCloseBatchDate==DateTime.MinValue?String.Empty:global_asax.lastExecutionCloseBatchDate.ToString("dd/MM/yyyy HH:mm")) %></td>
    </tr>				
  </tbody>
</table>
     <br />
    <br />
    <div style="text-align: center;">
        <span style="color:blue;font-size:20px">Statistiques</span>
    </div>
<table>
  <thead>
    <tr>
      <th>API</th>
      <th>Compteur</th>
      <th>Date de dernier appel</th>
    </tr>
  </thead>
  <tbody>
     <%foreach (Ceritek.WebServices.Biometrie.Models.WebServiceStats wstat in Ceritek.WebServices.Biometrie.Logic.WSSTAT.Instance.userStats){ %>
    <tr>
      <td><strong><%=wstat.Url %></strong></td>
      <td><%=wstat.Compteur%></td>
      <td><%=(wstat.LastDate==DateTime.MinValue?String.Empty:wstat.LastDate.ToString("dd/MM/yyyy HH:mm"))%></td>
    </tr>
      <%} %>	
     <%foreach (Ceritek.WebServices.Biometrie.Models.WebServiceStats wstat in Ceritek.WebServices.Biometrie.Logic.WSSTAT.Instance.rosettaStats){ %>
    <tr>
      <td><strong><%=wstat.Url %></strong></td>
      <td><%=wstat.Compteur%></td>
      <td><%=(wstat.LastDate==DateTime.MinValue?String.Empty:wstat.LastDate.ToString("dd/MM/yyyy HH:mm"))%></td>
    </tr>
      <%} %>	
     <%foreach (Ceritek.WebServices.Biometrie.Models.WebServiceStats wstat in Ceritek.WebServices.Biometrie.Logic.WSSTAT.Instance.batchStats){ %>
    <tr>
      <td><strong><%=wstat.Url %></strong></td>
      <td><%=wstat.Compteur%></td>
      <td><%=(wstat.LastDate==DateTime.MinValue?String.Empty:wstat.LastDate.ToString("dd/MM/yyyy HH:mm"))%></td>
    </tr>
      <%} %>	
  </tbody>
</table>
</body>
</html>
