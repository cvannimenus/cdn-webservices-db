﻿using Ceritek.WebServices.DB.Logic;
using Ceritek.WebServices.DB.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Ceritek.WebServices.DB.Controllers
{
    [EnableCorsAttribute("*", "*", "*")]
    [JSONContentTypeAtttribute]
    public class UserController : ApiController
    {
        [Route("api/customerhistory/{compte}")]
        [HttpGet]
        [BasicAuthentication]
        // GET: api/customerhistory/{compte}
        public History History([FromUri()] string compte)
        {
            LogInfo("[WEB] GET /api/customerhistory/" + compte);

            History ret = new History();
            SQL sqlLink = SQL.Instance;
            ret.history = sqlLink.getHistory(compte);         

            LogInfo("[WEB] --> " + ret.ToString());
            return ret;
        }

        [Route("api/customerhistory/{compte}/update")]
        [HttpPost]
        [BasicAuthentication]
        // GET: api/customerhistory/{compte}/update
        public bool SetAuth([FromUri()] string compte, [FromBody] SetAuth body)
        {
            LogInfo("[WEB] POST /api/customerhistory/" + compte + "/update");
            LogInfo("[WEB]" + body.ToString());
            bool ret = false;
            SQL sqlLink = SQL.Instance;
            ret = sqlLink.setAuth(compte, body);

            LogInfo("[WEB] --> " + ret);
            return ret;
        }

        [Route("api/customerhistory/{connid}/set")]
        [HttpPost]
        [BasicAuthentication]
        // GET: api/customerhistory/{connid}/set
        public bool SetAttributes([FromUri()] string connid, [FromBody] SetAttributes body)
        {
            LogInfo("[WEB] POST /api/customerhistory/" + connid + "/set");
            LogInfo("[WEB]" + body.ToString());
            bool ret = false;
            SQL sqlLink = SQL.Instance;
            ret = sqlLink.setAttributes(connid, body);

            LogInfo("[WEB] --> " + ret);
            return ret;
        }

        void LogInfo(string mess)
        {
            Global.WriteLogs(Global.LogLevel.INFO, "CustomerHistoryController", mess);
        }
    }
}
