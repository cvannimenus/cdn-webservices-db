﻿using Ceritek.WebServices.DB.Logic;
using Ceritek.WebServices.DB.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Ceritek.WebServices.DB.Controllers
{
    [EnableCorsAttribute("*", "*", "*")]
    [JSONContentTypeAtttribute]
    public class AgenceController : ApiController
    {

        [Route("api/agence/{code}")]
        [HttpGet]
        [BasicAuthentication]
        // GET: api/agence/{code}
        public Repertoire Repertoire([FromUri()] string code)
        {
            LogInfo("[WEB] GET /api/agence/" + code);

            Repertoire ret = new Repertoire();
            SQL sqlLink = SQL.Instance;
            ret.repertoire = sqlLink.getDirectory(code);

            LogInfo("[WEB] --> " + ret.ToString());
            return ret;
        }

        void LogInfo(string mess)
        {
            Global.WriteLogs(Global.LogLevel.INFO, "AgenceController", mess);
        }
    }
}
