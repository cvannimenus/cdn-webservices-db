﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace Ceritek.WebServices.DB.Models
{
    public class SetAttributes
    {
        [JsonProperty("authent_amer")]
        public string authent_amer { get; set; }
        [JsonProperty("client_delicat")]
        public string client_delicat { get; set; }
        [JsonProperty("stop_bvo_asked")]
        public string stop_bvo_asked { get; set; }
        [JsonProperty("delete_print_asked")]
        public string delete_print_asked { get; set; }

        public SetAttributes(string _Authentification_Americaine, string _Client_Delicat, string _Stop_BVO_Asked, string _Delete_Print_Asked)
        {
            authent_amer = _Authentification_Americaine;
            client_delicat = _Client_Delicat;
            stop_bvo_asked = _Stop_BVO_Asked;
            delete_print_asked = _Delete_Print_Asked;
        }
        public SetAttributes()
        {
        }

        public override string ToString()
        {
            return Json.Encode(this);
        }
    }
}