﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace Ceritek.WebServices.DB.Models
{
    public class Repertoire
    {
        [JsonProperty("repertoire")]
        public List<RepertoireLine> repertoire { get; set; }

        public Repertoire(List<RepertoireLine> _repertoire)
        {
            repertoire = _repertoire;
        }
        public Repertoire()
        {
            repertoire = new List<RepertoireLine>();
        }

        public override string ToString()
        {
            return Json.Encode(this);
        }
    }
}